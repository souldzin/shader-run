import { fromEvent } from "rxjs";
import { startWith, shareReplay, map } from "rxjs/operators";

const getWindowSize = () => ({
  width: window.innerWidth,
  height: window.innerHeight
});

const createWindowSizeObservable = () =>
  fromEvent(window, "resize").pipe(
    map(getWindowSize),
    startWith(getWindowSize()),
    shareReplay(1)
  );

export const windowSize$ = createWindowSizeObservable();
