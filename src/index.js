import * as THREE from "three";
import SPRITE_BLACK_SQUARE from "./textures/sprite_black_square.png";
import { windowSize$ } from "./behaviors";

const STAR_GROUP_RADIUS = 100;
const STAR_SIZE = 32;
const CUBE_COUNT = 3;

let views = [
  {
    left: 0,
    bottom: 0.5,
    width: 1.0,
    height: 0.5,
    background: new THREE.Color(0xffffff),
    setup: function(camera) {
      camera.layers.enable(0);
      camera.layers.enable(1);
    }
  },
  {
    left: 0,
    bottom: 0,
    width: 1.0,
    height: 0.5,
    background: new THREE.Color(0xffffff),
    setup(camera) {
      camera.applyMatrix(new THREE.Matrix4().makeScale(1, -1, 1));
      camera.layers.enable(0);
      camera.layers.enable(2);
    },
    invertY: true
  }
];

main();

function updateCameraAspect(view, rendererWidth, rendererHeight) {
  const width = Math.floor(rendererWidth * view.width);
  const height = Math.floor(rendererHeight * view.height);

  view.camera.left = -width / 2;
  view.camera.right = width / 2;
  view.camera.top = view.invertY ? 0 : height;
  view.camera.bottom = view.invertY ? -height : 0;
  view.camera.updateProjectionMatrix();
}

function main() {
  const container = document.getElementById("main");

  // renderer
  const renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);
  container.appendChild(renderer.domElement);

  // scene
  const scene = new THREE.Scene();
  scene.background = new THREE.Color(0xffffff);

  // textureLoader
  const textureLoader = new THREE.TextureLoader();

  // cameras
  views.forEach(view => {
    const camera = new THREE.OrthographicCamera(1, 1, 1, 1);
    camera.position.z = 500;

    if (view.setup) {
      view.setup(camera);
    }

    view.camera = camera;
  });

  // ctx
  const ctx = {
    renderer,
    scene,
    textureLoader,
    windowSize: { width: 0, height: 0 }
  };

  // entities
  const starGroup = createStarGroup(ctx);
  createGround(ctx);

  windowSize$.subscribe(size => {
    const { width, height } = size;
    ctx.windowSize = size;

    renderer.setSize(width, height);

    views.forEach(view => updateCameraAspect(view, width, height));
  });

  startAnimation((delta, time) => {
    starGroup.update(time);

    renderScene(ctx);
  });
}

function createStarGroup({ scene, textureLoader }) {
  // create sprites
  const mapStar = textureLoader.load(SPRITE_BLACK_SQUARE);
  const group = new THREE.Group();
  const materialStar = new THREE.SpriteMaterial({
    map: mapStar
  });
  for (var a = 0; a < CUBE_COUNT; a++) {
    var x = Math.random() - 0.5;
    var y = Math.random() - 0.5;
    var z = Math.random() - 0.5;
    var material;
    material = materialStar.clone();
    var sprite = new THREE.Sprite(material);
    sprite.position.set(x, y, z);
    sprite.position.normalize();
    sprite.position.multiplyScalar(STAR_GROUP_RADIUS);
    sprite.layers.set(a);
    group.add(sprite);
  }

  scene.add(group);
  group.position.y = STAR_GROUP_RADIUS * 2;

  return {
    update(time) {
      for (var i = 0, l = group.children.length; i < l; i++) {
        var sprite = group.children[i];
        var scale = Math.sin(time + sprite.position.x * 0.01) * 0.3 + 1.0;
        sprite.scale.set(scale * STAR_SIZE, scale * STAR_SIZE, 1.0);
      }
      group.rotation.x = time * 0.5;
      group.rotation.y = time * 0.75;
      group.rotation.z = time * 1.0;
    }
  };
}

function createGround({ scene }) {
  const material = new THREE.MeshBasicMaterial({ color: 0x000000 });
  const geometry = new THREE.BoxGeometry(10000, 50, 50);
  const ground = new THREE.Mesh(geometry, material);
  ground.position.set(0, 5, 0);

  scene.add(ground);
}

function startAnimation(onTick) {
  const clock = new THREE.Clock();

  const animate = () => {
    requestAnimationFrame(animate);

    onTick(clock.getDelta(), clock.getElapsedTime());
  };

  animate();
}

function renderScene({ renderer, scene, windowSize }) {
  const { width: rendererWidth, height: rendererHeight } = windowSize;

  views.forEach(view => {
    const left = Math.floor(rendererWidth * view.left);
    const bottom = Math.floor(rendererHeight * view.bottom);
    const width = Math.floor(rendererWidth * view.width);
    const height = Math.floor(rendererHeight * view.height);
    renderer.setViewport(left, bottom, width, height);
    renderer.setScissor(left, bottom, width, height);
    renderer.setScissorTest(true);
    renderer.render(scene, view.camera);
  });
}
